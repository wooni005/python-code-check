# python-code-check

Docker image to use in Gitlabs CI/CD pipeline to check the python code.

Contains pylint, flake8 and pyflakes
