FROM python:3.11-alpine3.18
WORKDIR /app
COPY requirements.txt /app
RUN apk --no-cache add --virtual .build build-base=0.5-r3 && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --no-cache del .build
